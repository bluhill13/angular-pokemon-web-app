import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  constructor() { }


  catch(pokemon: any){
    const ownedPokemonFromLocalStorage = localStorage.getItem('ownedPokemon');
    const ownedPokemon = ownedPokemonFromLocalStorage ? JSON.parse(ownedPokemonFromLocalStorage) : [];
    if (ownedPokemon.length === 0){
      ownedPokemon.push(pokemon);
      localStorage.setItem('ownedPokemon', JSON.stringify(ownedPokemon))
    } else if (this.checkIfAlreadyOwned(pokemon) !== true){
      ownedPokemon.push(pokemon);
      localStorage.setItem('ownedPokemon', JSON.stringify(ownedPokemon));
     } else {
      return false
    }
  }

  checkIfAlreadyOwned(pokemon): any{
    const ownedPokemonFromLocalStorage = localStorage.getItem('ownedPokemon');
    const ownedPokemon = ownedPokemonFromLocalStorage ? JSON.parse(ownedPokemonFromLocalStorage) : [];
    if(ownedPokemon.some(pk => pk.name === pokemon.name)){
      return true;
    } else{
       return false; 
    }

  }
}
