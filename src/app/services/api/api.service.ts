import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getAllPokemon(): Promise<any> {
    return this.http.get(environment.apiURL).toPromise();
  }

  loadNextPokemons(url: string): Promise<any> {
    return this.http.get(url).toPromise();
  }

  getPokemonByName(name: string): Promise<any> {
    return this.http.get(environment.apiURL + name).toPromise();
  }


  
}
