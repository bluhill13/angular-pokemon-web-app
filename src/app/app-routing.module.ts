import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { PokemonInfoComponent } from './components/pokemon-info/pokemon-info.component';
import { RegisterComponent } from './components/register/register.component';
import { TrainerpageComponent } from './components/trainerpage/trainerpage.component';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [

  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'trainerpage',
    component: TrainerpageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pokemon/:name',
    component: PokemonInfoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'register'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
