import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-trainerpage',
  templateUrl: './trainerpage.component.html',
  styleUrls: ['./trainerpage.component.scss']
})
export class TrainerpageComponent implements OnInit {

  public ownedPokemons: any = [];

  constructor(private session: SessionService, private route: Router) { 
  }

  ngOnInit(): void {
    this.ownedPokemons = localStorage.getItem('ownedPokemon') ? JSON.parse(localStorage.getItem('ownedPokemon')) : [];
  }

  get username(){
    return this.session.get().username;
  }

  onPokemonClicked(pk: any) {
    this.route.navigateByUrl(`pokemon/${pk.name}`);
  }

}
