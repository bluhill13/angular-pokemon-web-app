import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { SessionService } from 'src/app/services/session/session.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  constructor(private session: SessionService, private api: ApiService, private router: Router) { }

  public selectedPokemon: any = null;
  public pokemons: any[] = [];
  public next: any = null;

  ngOnInit(): void {
    this.loadPokemons()
  }

  async loadPokemons() {
    try {
      const result: any = await this.api.getAllPokemon();

      this.next = result.next;
      this.pokemons = result.results;

      this.pokemons.forEach((pokemon) => {
        const id = pokemon.url.split( '/' ).filter( Boolean ).pop();
        pokemon.imageUrl = `${environment.pokemonImgURL}${id}.png`;
      });    

    } catch (e) {
      console.error(e);
    }
  }
 
  onPokemonClicked(pk: any) {
    this.router.navigateByUrl(`pokemon/${pk.name}`);
  }

  async loadMore() {
    if (this.next !== null) {
      try {
        const result: any = await this.api.loadNextPokemons(this.next);

        this.next = result.next;
        const Pokemons = result.results;

        Pokemons.forEach((pokemon) => {
          const id = pokemon.url.split( '/' ).filter( Boolean ).pop();
          pokemon.imageUrl = `${environment.pokemonImgURL}${id}.png`;
        });   

        this.pokemons = this.pokemons.concat(Pokemons);

      } catch (e) {
        console.error(e);
      }
    }
  }



}
