import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';

@Component({
  selector: 'app-pokemon-info',
  templateUrl: './pokemon-info.component.html',
  styleUrls: ['./pokemon-info.component.scss']
})
export class PokemonInfoComponent implements OnInit {

  public name: string;
  public pokemonInfo: any;
  public alreadyOwned: boolean;
  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router, private trainerService: TrainerService) { }

  async ngOnInit() {
    this.loadPokemonInfo();
  }

  async loadPokemonInfo(){
    try {
      this.name = this.route.snapshot.paramMap.get('name');
      this.pokemonInfo = await this.api.getPokemonByName(this.name);
      this.alreadyOwned = this.trainerService.checkIfAlreadyOwned(this.pokemonInfo);
    } catch (e) {
      console.error(e);
    }

  }

  catch(){
    //create pokemon object to be stored in local storage
    const pokemonObject = {
      'name': this.pokemonInfo.name,
      'imageURL': this.pokemonInfo.sprites.front_default
    }
    this.trainerService.catch(pokemonObject)
    this.router.navigateByUrl('trainerpage')
  }

}
