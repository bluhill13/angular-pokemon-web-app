import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required,
                                    Validators.minLength(2)])
  });

  constructor(private session: SessionService, private router: Router) { 
    if (this.session.get() !== false){
      this.router.navigateByUrl('/trainerpage');
    }
  }

  ngOnInit(): void {
  }

  get username(){
    return this.loginForm.get('username');
  }

  onNameCreate() {
    console.log("button works")
    console.log(this.username.value)
    this.session.save({
      username: this.username.value,
      ownedPokemon: []
    });
    this.router.navigateByUrl('/trainerpage')
  }

}
